export interface User {
    deck:Array<number>,
    coins:number,
    name:string,
    token:string
}
