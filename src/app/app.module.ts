import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { StoreComponent } from './components/store/store.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { DeckComponent } from './components/deck/deck.component';

import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DeckFilterPipe } from './pipes/deck-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    PokemonCardComponent,
    StoreComponent,
    NavigationComponent,
    DeckComponent,
    DeckFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
