import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pokemon } from "../../interfaces/pokemon";


@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {
  @Input() data:Pokemon;
  @Output() sellingCard = new EventEmitter<number>();
  @Output() addedCard = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
    
  }
  sellCard(){
    this.sellingCard.emit(this.data.poke_id);
  }
  addCardToDeck(){
    this.addedCard.emit(this.data.poke_id);
  }
}
