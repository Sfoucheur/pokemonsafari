import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { UserApiService } from '../../services/user-api.service';
import { SharedDataService } from '../../services/shared-data.service';
import { User } from "../../interfaces/user";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  
  user:User;
  constructor(private router:Router,private userApi:UserApiService,private sharedApi: SharedDataService) {
    this.user = this.sharedApi.getUser();
  }

  ngOnInit() {
  }
  onDeleteUser(){
    var userstorage = JSON.parse(localStorage.getItem("pokeUsers"));
    this.userApi.deleteUser(this.user.token).then(
      data => {
        delete userstorage[this.user.name];
        localStorage.setItem("pokeUsers",JSON.stringify(userstorage));
        this.router.navigate([""]);
      },
      err => {
        console.log(JSON.stringify(err));
      }
    );
  }
}
