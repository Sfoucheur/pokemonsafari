import { Component, OnInit } from '@angular/core';
import { PokemonApiService } from '../../services/pokemon-api.service';
import { SharedDataService } from '../../services/shared-data.service';
import { Pokemon } from "../../interfaces/pokemon";

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss']
})
export class DeckComponent implements OnInit {
  data: Array<Pokemon>;
  constructor(private sharedApi:SharedDataService,private pokeApi:PokemonApiService) { }

  ngOnInit() {
    this.data=[];
    let pokemons = this.sharedApi.getUser()["deck"];
    for(let i = 0; i<pokemons.length;i++){
      this.pokeApi.search(pokemons[i]).then((data: Pokemon) => {
        this.data.push(data);
      });
    }
  }

}



