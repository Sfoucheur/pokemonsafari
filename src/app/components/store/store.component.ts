import {
  Component,
  OnInit
} from '@angular/core';
import {
  PokemonApiService
} from '../../services/pokemon-api.service';
import {
  SharedDataService
} from '../../services/shared-data.service';
import {
  UserApiService
} from '../../services/user-api.service';
import {
  User
} from "../../interfaces/user";
import {
  Pokemon
} from "../../interfaces/pokemon";

import Swal from 'sweetalert2';



@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  data: Array < Pokemon > = [];
  user: User;

  constructor(private pokeApi: PokemonApiService, private sharedApi: SharedDataService, private userApi: UserApiService) {
    this.user = this.sharedApi.getUser();
  }

  ngOnInit() {

  }
  OnClick() {
    if (this.user.coins >= 10) {
      this.user.coins -= 10;
      this.sharedApi.setUser(this.user);
      this.userApi.updateUser(this.user.token, this.user.coins, this.user.deck);
      let temp: Array < Pokemon >= [];
      for (let j = 0; j < 10; j++) {
        this.pokeApi.search(Math.floor(Math.random() * 200) + 1).then(
          (data: Pokemon) => {
            temp.push(data);
          },
          err=>{
            console.log(err);
          });
      }
      this.data = temp;
    }else{
      Swal.fire(
        'Pas assez de PK',
        'Un paquet de carte coûte 10 PK !',
        'error'
      )
    }





  }
  soldCard(poke_id,arrayId) {
    this.data.splice(arrayId, 1);
    this.user.coins += 0.5;
    this.userApi.updateUser(this.user.token, this.user.coins, this.user.deck);
    this.sharedApi.setUser(this.user);

  }
  addedCard(poke_id,arrayId) {
    this.user.deck.push(poke_id);
    this.userApi.updateUser(this.user.token, this.user.coins, this.user.deck);
    this.sharedApi.setUser(this.user);
    this.data.splice(arrayId, 1);
  }

}
