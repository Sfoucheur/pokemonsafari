import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { UserApiService } from '../../services/user-api.service';
import { SharedDataService } from '../../services/shared-data.service';
import { User } from "../../interfaces/user";


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  model:{name:string};
  constructor(private router: Router,private userApi: UserApiService,private sharedApi: SharedDataService) {
    this.model={name:""};
  }
  login() {
    var userName = this.model.name;
    if(localStorage.length==0){
      localStorage.setItem("pokeUsers",JSON.stringify({}));
    }
    var storage = JSON.parse(localStorage.getItem("pokeUsers"));
    var token = storage[userName];
    if(!token)
    { 
      this.userApi.login(userName).then((data: any) => {
          var token = data.token;
          this.userApi.getUser(token).then(
            data => {
              let user:User={
                name:userName,
                coins:data["coins"],
                deck:data["deck"],
                token:token
              };
              this.sharedApi.setUser(user);
              storage[user.name]=user.token;
              localStorage.setItem("pokeUsers",JSON.stringify(storage));
              this.router.navigate(["/home"]);
            },
            err => {
              console.log(JSON.stringify(err)+"token : "+data.token);
            }
          );
          
        });
    }
    else{
          this.userApi.getUser(token).then((data:User)=>{
            let user:User={
              name:userName,
              coins:data["coins"],
              deck:data["deck"],
              token:token
            };
            this.sharedApi.setUser(user);
            this.router.navigate(["/home"]);
          });
    }
    
  }
  ngOnInit() {
    
  }

}
