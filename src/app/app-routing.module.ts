import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginFormComponent } from './components/login-form/login-form.component';
import { StoreComponent } from './components/store/store.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { DeckComponent } from './components/deck/deck.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';


const routes: Routes = [
  { path: '', component: LoginFormComponent },
  {
    path: 'home', component: NavigationComponent,
    children: [
      { path: '', redirectTo: 'deck', pathMatch: 'full' },
      { path: 'store', component: StoreComponent },
      { path: 'deck', component: DeckComponent }
    ],
    canActivate: [
      AuthGuardGuard
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
