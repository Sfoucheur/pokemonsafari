import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpHeaders
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {
  BASEURL = 'http://lpweblannion.herokuapp.com/api/user';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  };
  constructor(private http: HttpClient) {}

  login(username: string) {
    let body = {name:username};
    return this.http.post(this.BASEURL + '/login', body).toPromise();
  }
  getUser(token: string) {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', token);
    return this.http.get(this.BASEURL, this.httpOptions).toPromise();
  }
  deleteUser(token: string) {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', token);
    return this.http.delete(this.BASEURL, this.httpOptions).toPromise();
  }
  updateUser(token: string, coins: number, pokemons: Array < number > ) {
    var body = {
      coins: coins,
      deck: pokemons
    };
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', token);
    return this.http
      .put(this.BASEURL, body, {
        headers: this.httpOptions.headers
      })
      .subscribe(
        data => {
          console.log(data);
        },
        err => {
          console.log(err);
        }
      );
  }
}


