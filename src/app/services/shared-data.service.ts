import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
  user:User;
  constructor() { }
  setUser(user:User){
    this.user=user;
  }
  getUser(){
    return this.user;
  }
}
