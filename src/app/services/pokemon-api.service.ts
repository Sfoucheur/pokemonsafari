import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {
  BASEURL = 'http://lpweblannion.herokuapp.com/api/pokemon/';
  constructor(private http: HttpClient) { }

  search(pokeId: number){
    return this.http.get(this.BASEURL + pokeId).toPromise();
  }
  
}
