import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from "../interfaces/pokemon";

@Pipe({
  name: 'deckFilter'
})
export class DeckFilterPipe implements PipeTransform {

  transform(cards: Array<Pokemon>, searchText:number): Array<Pokemon> {
      if(!cards)return [];
      if(!searchText) return cards;
      return cards.filter( card => {
        console.log(card);
        if(card.stats.attack>=searchText)
          return card;
      });
  }
}

